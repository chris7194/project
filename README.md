**Welcome to Auto Imports**
--
*Justification for the method or framework used*
--
Auto Imports website was created using C# on Microsoft Visual Studio 2017 and Microsoft SQL Server Management Studio 2017. The Website was developed using a 3-tier model. This makes the website better in performance, higher availability and provides greater scalability. The 3-tier model is made up of the Website itself, the Business Logic, the Data Access and Common.
•	Business Logic - This is where the processing and filtering of data is happening before it is passed to the DataAccess.
•	DataAccess - This is where the transition of the data is brought and sent to the database.
•	Common - This is where the database connection string and other useful or important classes are found. The encryption class is held in the common. Also, the model is present in the Common.

*Description of the proposed system*
--
This web-based application will include useful information for all types of vehicles. The system will be running: Search, Add (admin only), Edit (admin only), and Delete (admin only) functionality.                     In the inventory system the website will be running a database of vehicles using Microsoft SQL Server.
Fields in the inventory which will be available to view from are as follows: 
•	Color
•	Chassis number (only available for users with Admin Rights)
•	Engine Size
•	Kilometers
•	Cost (How much it cost AutoImports to purchase the vehicle including all import charges) (only available for users with Admin Rights)
•	Asking Price (Recommended Retail Price)
•	Deposit (Minimum Deposit Price)
•	Minimum Price (This is the minimum price the vehicle can be sold at) (only available for users with Admin Rights)
•	Reserved status (This shows if the car is reserved or not)
•	Model Name
•	Manufacturer Name

