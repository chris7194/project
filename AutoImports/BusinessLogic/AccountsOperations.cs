﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class AccountsOperations
    {
        /// <summary>
        /// Generates a token used to recover account or chnage password
        /// </summary>
        /// <param name="length">Length of token</param>
        /// <returns>Randomised Token</returns>
        public string GeneratePasswordRecoveryToken(int length)
        {
            Random random = new Random();
            string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }
    }
}