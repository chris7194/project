﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataAccess;

namespace BusinessLogic
{
    public class UsersOperations
    {
        #region Read Operations

        /// <summary>
        /// Gets all users from Database table: Users
        /// </summary>
        /// <returns>List of users of type Users</returns>
        public List<User> GetAllUsers()
        {
            UsersRepository ur = new UsersRepository();
            return ur.GetUsers();
        }


        /// <summary>
        /// Search users by any name or surname
        /// </summary>
        /// <param name="keyword">Keyword use to search</param>
        /// <returns>List of users of type Users</returns>
        public List<User> Search(string keyword)
        {
            UsersRepository ur = new UsersRepository();
            if (string.IsNullOrEmpty(keyword))
            {
                return ur.GetUsers();
            }
            else
            {
                return ur.GetUsers(keyword);
            }
        }

        /// <summary>
        /// Check if user has a matching recovery token
        /// </summary>
        /// <param name="user">Username</param>
        /// <param name="req">Recovery token</param>
        /// <returns>True or False</returns>
        public bool CheckPassReq(string user, string req)
        {
            UsersRepository ur = new UsersRepository();
            return ur.CheckPassReq(user, req);
        }

        /// <summary>
        /// Updates or sets recovery token to user
        /// </summary>
        /// <param name="u">User</param>
        /// <param name="req">Recovery token</param>
        public void PasswordRequest(User u, string req)
        {
            UsersRepository ur = new UsersRepository();
            ur.PasswordRequest(u, req);
        }

        /// <summary>
        /// Delete user token
        /// </summary>
        /// <param name="u">User</param>
        public void DeletePasswordRequest(User u)
        {
            UsersRepository ur = new UsersRepository();
            ur.DeletePasswordRequest(u);
        }

        /// <summary>
        /// Check if username exists
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>True or False</returns>
        public bool CheckUser(string username)
        {
            return new UsersRepository().doesUsernameExist(username);
        }

        /// <summary>
        /// Check if user has matching email address
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="email">Email</param>
        /// <returns>True or False</returns>
        public bool CheckUserEmail(string username, string email)
        {
            if (CheckUser(username) == true)
            {
                if (new UsersRepository().CheckUserEmail(username, email))
                {
                    return true;
                }
                else
                {
                    throw new Exception("Email does not exist");
                }

            }
            else
            {
                throw new Exception("Username does not exist");
            }
        }

        /// <summary>
        /// Get user of type User
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>User of type User</returns>
        public User GetUser(string username)
        { 
            return new UsersRepository().GetUser(username);
        }

        /// <summary>
        /// Check if login credentials match
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>True or False</returns>
        public bool Login(string username, string password)
        {
            Encryption e = new Encryption();
            string hashpass = e.HashString(password);
            return new UsersRepository().Login(username, hashpass);
        }

        #endregion



        #region Write Operations

        /// <summary>
        /// Adds users credentials in database
        /// </summary>
        /// <param name="u">User</param>
        public void Register(User u)
        {
            UsersRepository ur = new UsersRepository();
            Encryption e = new Encryption();
            string hashpass = e.HashString(u.Password);
            if (ur.doesUsernameExist(u.Username))
            {
                throw new UsernameExistsException("Username alreays exists");
            }
            else
            {
                u.Password = hashpass;
                ur.addUser(u);
                Role r = ur.GetRole(2);
                ur.AllocateRoleToUser(u, r);
            }
        }

        /// <summary>
        /// Deletes user
        /// </summary>
        /// <param name="username">Username</param>
        public void Delete(string username)
        {
            UsersRepository ur = new UsersRepository();
            if (ur.doesUsernameExist(username))
            {
                ur.Delete(ur.GetUser(username));
            }
            else throw new UsernameExistsException("Username does not exist, Delte failed");
        }

        /// <summary>
        /// Add Role to user
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="roleId">Role Id</param>
        public void AllocateRoleUser(string username, int roleId)
        {
            UsersRepository ur = new UsersRepository();
            User u = ur.GetUser(username);
            Role r = ur.GetRole(roleId);
            if (ur.IsUserAllocatedRole(u, r))
            {
                throw new Exception("User cannot be allocated to role because he already has that role");
            }
            else
            {
                ur.AllocateRoleToUser(u, r);
            }
        }

        /// <summary>
        /// Removes Role from user
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="roleId">Role Id</param>
        public void DeallocateRoleUser(string username, int roleId)
        {
            UsersRepository ur = new UsersRepository();
            User u = ur.GetUser(username);
            Role r = ur.GetRole(roleId);

            if (ur.IsUserAllocatedRole(u, r))
            {
                ur.Deallocate(u, r);
            }
        }

        /// <summary>
        /// Modify or change users password
        /// </summary>
        /// <param name="u">User</param>
        /// <param name="newpassword">New Password</param>
        public void ChangePassword(User u, string newpassword)
        {
            UsersRepository ur = new UsersRepository();
            u.Password = newpassword;
            ur.ChangePassword(u);
        }

        #endregion 
    }
}
