﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataAccess;

namespace BusinessLogic
{
    public class ModelOperations
    {
        #region Write Operations
        public List<Model> GetAllModels()
        {
            ModelRepository mr = new ModelRepository();
            return mr.GetModels();
        }

        public List<Model> Search(string keyword)
        {
            ModelRepository mr = new ModelRepository();
            if (string.IsNullOrEmpty(keyword))
            {
                return mr.GetModels();
            }
            else
            {
                return mr.GetModels(keyword);
            }
        }

        public Model GetModel(int ModelId)
        {
            return new ModelRepository().GetModel(ModelId);
        }
        #endregion
        #region Write Operations
        public void RegisterModel(Model m)
        {
            ModelRepository mr = new ModelRepository();

        }
        #endregion
    }
}
