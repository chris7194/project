﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Common;

namespace BusinessLogic
{
    public class ManufacturerOperations
    {
        #region Write Operations
        public List<Manufacturer> GetAllManufacturers()
        {
            ManufacturerRepository mr = new ManufacturerRepository();
            return mr.GetManufacturers();
        }

        public List<Manufacturer> Search(string keyword)
        {
            ManufacturerRepository mr = new ManufacturerRepository();
            if (string.IsNullOrEmpty(keyword))
            {
                return mr.GetManufacturers();
            }
            else
            {
                return mr.GetManufacturers(keyword);
            }
        }

        public Manufacturer GetManufacturer(int manufacturer)
        {
            return new ManufacturerRepository().GetManufacturer(manufacturer);
        }
        #endregion
        #region Write Operations
        public void RegisterManufacturer(Manufacturer m)
        {
            ManufacturerRepository mr = new ManufacturerRepository();
            
        }
        #endregion

    }
}
