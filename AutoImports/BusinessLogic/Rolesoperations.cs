﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataAccess;

namespace BusinessLogic
{
    public class RolesOperations
    {
        /// <summary>
        /// Get List of Roles
        /// </summary>
        /// <returns>List of role of type Roles</returns>
        public List<Role> GetAllRoles()
        {
            return new RolesRepository().GetAllRoles();
        }
    }
}
