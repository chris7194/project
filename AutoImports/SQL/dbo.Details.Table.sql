USE [Autoimports2019]
GO
/****** Object:  Table [dbo].[Details]    Script Date: 15/02/2019 04:45:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Details](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Color] [nvarchar](50) NULL,
	[Chassis] [nvarchar](50) NULL,
	[Year Manufactured] [int] NULL,
	[Engine_Size] [nvarchar](50) NULL,
	[Type Of Vehicle] [nvarchar](50) NULL,
	[Kilometers] [int] NULL,
	[Cost] [int] NULL,
	[Asking_Price] [int] NULL,
	[Deposit] [int] NULL,
	[Min_Price] [int] NULL,
	[Reserved] [bit] NULL,
	[ImagePath] [text] NULL,
	[Model_Id] [int] NULL,
 CONSTRAINT [PK_Details] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Details]  WITH CHECK ADD  CONSTRAINT [FK_Details_Model] FOREIGN KEY([Model_Id])
REFERENCES [dbo].[Model] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Details] CHECK CONSTRAINT [FK_Details_Model]
GO
