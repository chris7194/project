﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using BusinessLogic;
using System.Net.Mail;

namespace AutoImports.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ContactMessage c)
        {
            new ContactOperations().postmessage(c);
            string token = new AccountsOperations().GeneratePasswordRecoveryToken(25);

            MailMessage mail = new MailMessage();
            mail.To.Add("autoimportsnoreply@gmail.com");
            mail.From = new MailAddress("autoimportsnoreply@gmail.com");
            mail.Subject = c.Subject + " - " + c.Email;
            string body = c.Message;
            mail.Body = body;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.Credentials = new System.Net.NetworkCredential("autoimportsnoreply@gmail.com", "C0mPuTeR!ProjEct2019");
            smtp.Send(mail);
            ViewData["info"] = "Email sent to " + c.Email;
            ViewData["success"] = "Thank you for contacting us. Your message has been sent we will contact you shortly.";
            return View();
        }
    }
}