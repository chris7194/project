﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;
using Common;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace ClassExample.Controllers
{
    public class UsersController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            UsersOperations uo = new UsersOperations();//businesslogic class
            var myList = uo.GetAllUsers();
            return View(myList);
        }

        public ActionResult Search(string keyword) {
            UsersOperations uo = new UsersOperations();
            var myFilteredList = uo.Search(keyword);
            return View("Index", myFilteredList);

        }

        public ActionResult Details(string username) {
            return View(new UsersOperations().GetUser(username));
        }

        [HttpGet]
        public ActionResult Register() {//Purpose of this method is to render or load where the user can type the values or the input
            return View();//loads page
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User u, HttpPostedFileBase profilepicture) {
            UsersOperations uo = new UsersOperations();
            try
            {
                if (ModelState.IsValid)
                {
                    if (Regex.IsMatch(u.Password, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,15}$"))
                    {
                        if (profilepicture != null)
                        {
                            //validation whether file is an actual image or not
                            if (profilepicture.ContentLength <= (1048576 * 3))
                            {
                                //FF D8
                                byte[] whitelist = new byte[] { 255, 216 };
                                byte[] inputRead = new byte[2];
                                profilepicture.InputStream.Read(inputRead, 0, 2);
                                bool flag = true;
                                for (int i = 0; i < 2; i++)
                                {
                                    if (whitelist[i] != inputRead[i])
                                    {
                                        flag = false;
                                        break;
                                    }
                                }
                                if (flag == true)
                                {
                                    string absolutePath = Server.MapPath("\\UserImages\\"); //to save physical file
                                    string relativePath = "\\UserImages\\"; //to save the path in db and to render the image

                                    string fileName = Guid.NewGuid().ToString() + Path.GetExtension(profilepicture.FileName);
                                    u.ImagePath = relativePath + fileName; //saves path to the image in db
                                    uo.Register(u);
                                    profilepicture.SaveAs(absolutePath + fileName);//Save image in folder
                                }
                            }
                        }
                        else
                        {
                            uo.Register(u);
                        }

                        ViewData["success"] = "\"" + u.Username + "\" has been registered successfully";
                        ModelState.Clear(); //Clears errors() textboxes
                    }
                    else
                    {
                        ModelState.AddModelError("Password", "Password must be at least 8 characters, no more than 15 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit.");
                    }
                }
            }


            catch (UsernameExistsException ex)
            {
                ModelState.AddModelError("Username", ex.Message);//coming from the model
                ViewData["message"] = ex.Message;//coming from the class
            }
            catch(Exception e)
            {
                ViewData["message"] = "User registration failed. Try again or contact admin. " + e;
            }
            return View("../Accounts/Login");
        }

        [Authorize(Roles = "admin")]
        public ActionResult Delete(string username) {
            UsersOperations uo = new UsersOperations();
            try{
                uo.Delete(username);
                ViewData["message"] = "user deleted successfully";
            }
            catch (Exception ex) {
                ViewData["message"] = "user was not deleted" + ex;
            }
            
            var myList = uo.GetAllUsers();
            return View("Index",myList);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult RolesAllocation(){
            return View();//ha mmur go page li jisima Register (listess bhal method)
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult RolesAllocation(string username, int roleId, string choice)
        {
            UsersOperations uo = new UsersOperations();
            try
            {
                if (choice == "Allocate")
                {
                    uo.AllocateRoleUser(username, roleId);
                    ViewData["message"] = "Role has been allocated to user";
                }
                else {
                    uo.DeallocateRoleUser(username, roleId);
                    ViewData["message"] = "Role has been deallocated";
                }
                
            }
            catch (Exception e) {
                ViewData["message"] = e.Message;
            }

            return View();//ha mmur go page li jisima Register (listess bhal method)
        }
    }
}