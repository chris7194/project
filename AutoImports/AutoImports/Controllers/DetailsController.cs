﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Common;

namespace AutoImports.Controllers
{
    public class DetailsController : Controller
    {
        private Autoimports2019Entities db = new Autoimports2019Entities();

        // GET: Details
        public ActionResult Index()
        {
            var details = db.Details.Include(d => d.Model);
            return View(details.ToList());
        }

        // GET: Details/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detail detail = db.Details.Find(id);
            if (detail == null)
            {
                return HttpNotFound();
            }
            if (detail.ImagePath == null) {
                detail.ImagePath = "\\Images\\defaultcar.png";
                return View(detail);
            }
            return View(detail);
        }

        // GET: Details/Create
        public ActionResult Create(int modid)
        {
            ViewBag.Model_Id = new SelectList(db.Models, "Id", "Name");
            ViewBag.modid = modid;
            return View();
        }

        // POST: Details/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Detail detail, HttpPostedFileBase picture)
        {
            if (ModelState.IsValid)
            {
                if (picture != null)
                {
                    //validation whether file is an actual image or not
                    if (picture.ContentLength <= (1048576 * 3))
                    {
                        //FF D8
                        byte[] whitelist = new byte[] { 255, 216 };
                        byte[] inputRead = new byte[2];
                        picture.InputStream.Read(inputRead, 0, 2);
                        bool flag = true;
                        for (int i = 0; i < 2; i++)
                        {
                            if (whitelist[i] != inputRead[i])
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag == true)
                        {
                            string absolutePath = Server.MapPath("\\Images\\"); //to save physical file
                            string relativePath = "\\Images\\"; //to save the path in db and to render the image
                            string fileName = Guid.NewGuid().ToString() + Path.GetExtension(picture.FileName);
                            detail.ImagePath = relativePath + fileName; //saves path to the image in db
                            db.Details.Add(detail);
                            db.SaveChanges();
                            picture.SaveAs(absolutePath + fileName);//Save image in folder
                            return RedirectToAction("Index");
                        }
                    }
                }
            }

            ViewBag.Model_Id = new SelectList(db.Models, "Id", "Name", detail.Model_Id);
            return View(detail);
        }

        // GET: Details/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detail detail = db.Details.Find(id);
            if (detail == null)
            {
                return HttpNotFound();
            }
            ViewBag.Model_Id = new SelectList(db.Models, "Id", "Name", detail.Model_Id);
            return View(detail);
        }

        // POST: Details/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Detail detail)
        {
            if (ModelState.IsValid)
            {
                db.Details.AddOrUpdate(detail);
                //db.Entry(detail).State = EntityState.Modified;
                db.SaveChanges();
                Detail details = db.Details.Find(detail.Id);
                return View("Details", details);
            }
            ViewBag.Model_Id = new SelectList(db.Models, "Id", "Name", detail.Model_Id);
            return View(detail);
        }

        // GET: Details/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detail detail = db.Details.Find(id);
            if (detail == null)
            {
                return HttpNotFound();
            }
            return View(detail);
        }

        public ActionResult GetDetails(int id) {
            Detail detail = db.Details.SingleOrDefault(x => x.Model_Id == id);
            return View("Details",detail);
        }

        // POST: Details/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Detail detail = db.Details.Find(id);
            db.Details.Remove(detail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
