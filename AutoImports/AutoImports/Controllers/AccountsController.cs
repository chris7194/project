﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;
using System.Web.Security;
using System.Net.Mail;
using Common;
using System.Text.RegularExpressions;

namespace ClassExample.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Accounts
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            if (!User.Identity.IsAuthenticated)
            {
                if (new UsersOperations().Login(username, password) == true)
                {
                    //log in
                    FormsAuthentication.SetAuthCookie(username, true);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewData["message"] = "There are no accounts with those credentials. Please check your username and password again.";
                }
            }
            return View();
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {

                FormsAuthentication.SignOut();
            }
            return RedirectToAction("Register", "Users");
        }

        public ActionResult RecoverPassword(string username, string email)
        {
            UsersOperations uo = new UsersOperations();
            try
            {
                if (uo.CheckUserEmail(username, email))
                {
                    string token = new AccountsOperations().GeneratePasswordRecoveryToken(25);
                    User u = uo.GetUser(username);
                    uo.PasswordRequest(u, token);

                    MailMessage mail = new MailMessage();
                    mail.To.Add(email);
                    mail.From = new MailAddress("autoimportsnoreply@gmail.com");
                    mail.Subject = "Forgot Password";
                    string body = "Please go to this address to recover your password /Accounts/ChangePassword/?user=" + username + "&req=" + token;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new System.Net.NetworkCredential("autoimportsnoreply@gmail.com", "C0mPuTeR!ProjEct2019");
                    smtp.Send(mail);                                    
                    ViewData["info"] = "Email sent to " + email;
                }
                else
                {
                    ViewData["message"] = "Something went wrong";
                }
            }
            catch (Exception e)
            {
                ViewData["message"] = e.Message;
            }
            if (User.Identity.IsAuthenticated) {
                TempData["message"] = "Password recovery has been sent.";
                return RedirectToAction("Index", "Users");
            }
            return View("Login");
        }

        public ActionResult ChangePassword(string user, string req)
        {
            UsersOperations uo = new UsersOperations();
            try
            {
                bool a = true;
                a = uo.CheckPassReq(user, req);
                if (uo.CheckUser(user))
                {
                    if (uo.CheckPassReq(user, req))
                    {
                        ViewData["info"] = "Please choose a strong password";
                    }
                    else
                    {
                        ViewData["message"] = "Something went wrong, this could be because the user does not exist or the link you entered is incorrect. Please try again.";
                    }
                }
            }
            catch (Exception e)
            {
                ViewData["message"] = "Something went wrong! Please go back to the previous page." + e;
            }
            return View();
        }

        public ActionResult SubmitNewPassword(string username, string email, string password)
        {
            UsersOperations uo = new UsersOperations();
            if (uo.CheckUser(username))
            {
                User u = uo.GetUser(username);
                if (uo.CheckUserEmail(username, email))
                {
                    if (Regex.IsMatch(password, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,15}$"))
                    {
                        uo.ChangePassword(u, new Encryption().HashString(password));
                        u.PasswordReqCode = null;
                        uo.DeletePasswordRequest(u);
                    }
                    else
                    {
                        ModelState.AddModelError("Password", "Password must be at least 8 characters, no more than 15 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit.");
                        ViewData["message"] = "Password must be at least 8 characters, no more than 15 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit.";
                        return View("Login");
                    }
                }
                else
                {
                    ViewData["message"] = "Email enetered doesn't match";
                }
            }
            else
            {
                ViewData["message"] = "User does not exist";
            }
            ViewData["success"] = "You have successfully changed your password";
            return View("Login");
        }

    }
}