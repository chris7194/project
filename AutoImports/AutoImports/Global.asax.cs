﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AutoImports
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //Gib ir roles 
            if (Context.User != null)
            {
                if (Context.User.Identity.IsAuthenticated)
                {
                    //1. Get list of roles for the given user
                    var list = new BusinessLogic.UsersOperations().GetUser(Context.User.Identity.Name).Roles;

                    //2. Input/Save that list of roles into the Context.User
                    string[] roles = new string[list.Count];
                    for (int i = 0; i < roles.Length; i++)
                    {
                        roles[i] = list.ElementAt(i).Title;
                    }
                    GenericPrincipal gp = new GenericPrincipal(Context.User.Identity, roles);
                    Context.User = gp;

                }
            }
        }
    }
}
