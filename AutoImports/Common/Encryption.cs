﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Encryption
    {
        /// <summary>
        /// Method to hash string for security purposes
        /// </summary>
        /// <param name="mytext">Any string given</param>
        /// <returns>Hashed string</returns>
        public string HashString(string mytext)
        {
            SHA256 myAlg = SHA256.Create();//Initializing the algorithm
            byte[] input = Encoding.UTF32.GetBytes(mytext);//converting from string to byte[]
            byte[] digest = myAlg.ComputeHash(input);//hashing bute[] base64 bytes
            return Convert.ToBase64String(digest);//converting back from byte[] to string
        }
    }
}
