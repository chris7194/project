﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class UsersRepository: ConnectionClass
    {
        /// <summary>
        /// Calling the base class which is the connection class to get 
        /// connection string inside users repository controller.
        /// </summary>
        public UsersRepository() : base() { }

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns>List of users of type Users</returns>
        public List<User> GetUsers()
        {
            return Entity.Users.ToList();
        }

        /// <summary>
        /// Get users by name or surname
        /// </summary>
        /// <param name="keyword">Name or surname</param>
        /// <returns>List of users of type Users</returns>
        public List<User> GetUsers(string keyword)
        {
            return Entity.Users.Where(x => x.Name.Contains(keyword) || x.Surname.Contains(keyword)).ToList();
        }

        /// <summary>
        /// Adds Users to database table
        /// </summary>
        /// <param name="u">User</param>
        public void addUser(User u)
        {
            Entity.Users.Add(u);
            Entity.SaveChanges();
        }

        /// <summary>
        /// Get users by username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>User</returns>
        public User GetUser(string username)
        {
            return Entity.Users.SingleOrDefault(x => x.Username == username);
        }
               
        /// <summary>
        /// Validates if user has matching email
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="email">Email</param>
        /// <returns>True or False</returns>
        public bool CheckUserEmail(string username, string email)
        {
            return Entity.Users.SingleOrDefault(x => x.Username == username && x.Email == email) == null ? false : true;
        }

        /// <summary>
        /// Check if user has matching recovery token
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="req">Token</param>
        /// <returns></returns>
        public bool CheckPassReq(string user, string req)
        {
            return Entity.Users.SingleOrDefault(x => x.Username == user && x.PasswordReqCode == req) == null ? false : true;
        }

        /// <summary>
        /// Updates or adds recovery token to user
        /// </summary>
        /// <param name="u">User</param>
        /// <param name="req">Recovery token</param>
        public void PasswordRequest(User u, string req)
        {
            u.PasswordReqCode = req;
            Entity.Users.AddOrUpdate(u);
            Entity.SaveChanges();
        }

        /// <summary>
        /// Delete user password request
        /// </summary>
        /// <param name="u">User</param>
        public void DeletePasswordRequest(User u)
        {
            Entity.Users.AddOrUpdate(u);
            Entity.SaveChanges();
        }

        /// <summary>
        /// Check if user with username exists
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>True or False</returns>
        public bool doesUsernameExist(string username)
        {
            if (GetUser(username) == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Adds role to user
        /// </summary>
        /// <param name="u">User</param>
        /// <param name="r">Role</param>
        public void AllocateRoleToUser(User u, Role r)
        {
            u.Roles.Add(r);
            Entity.SaveChanges();
        }

        /// <summary>
        /// Deletes role from user
        /// </summary>
        /// <param name="u">User</param>
        /// <param name="r">Role</param>
        public void Deallocate(User u, Role r)
        {
            u.Roles.Remove(r);
            Entity.SaveChanges();
        }

        /// <summary>
        /// Check user role
        /// </summary>
        /// <param name="u">User</param>
        /// <param name="r">Role</param>
        /// <returns></returns>
        public bool IsUserAllocatedRole(User u, Role r)
        {
            return u.Roles.Contains(r);
        }

        /// <summary>
        /// Get role by role id
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Role</returns>
        public Role GetRole(int id)
        {
            return Entity.Roles.SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Deletes user
        /// </summary>
        /// <param name="u">User</param>
        public void Delete(User u)
        {
            Entity.Users.Remove(u);
            Entity.SaveChanges();
        }

        /// <summary>
        /// Check users credentials to login user
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public bool Login(string username, string password)
        {
            return Entity.Users.SingleOrDefault(x => x.Username == username && x.Password == password) == null ? false : true;
        }

        /// <summary>
        /// Modify password
        /// </summary>
        /// <param name="u">User</param>
        public void ChangePassword(User u)
        {
            Entity.Users.AddOrUpdate(u);
            Entity.SaveChanges();
        }

    }
}