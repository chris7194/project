﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class ManufacturerRepository: ConnectionClass
    {
        public ManufacturerRepository() : base() { }

        public List<Manufacturer> GetManufacturers()
        {
            return Entity.Manufacturers.ToList();
        }

        public List<Manufacturer> GetManufacturers(string keyword)
        {
            return Entity.Manufacturers.Where(x => x.Name.Contains(keyword)).ToList();
        }

        public void addUser(Manufacturer u)
        {
            Entity.Manufacturers.Add(u);
            Entity.SaveChanges();
        }

        public Manufacturer GetManufacturer(int id)
        {
            return Entity.Manufacturers.SingleOrDefault(x => x.Id == id);
        }
    }
}
