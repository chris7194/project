﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class ModelRepository : ConnectionClass
    {
        public ModelRepository() : base() { }

        public List<Model> GetModels()
        {
            return Entity.Models.ToList();
        }

        public List<Model> GetModels(string keyword)
        {
            return Entity.Models.Where(x => x.Name.Contains(keyword)).ToList();
        }

        public void addUser(Model u)
        {
            Entity.Models.Add(u);
            Entity.SaveChanges();
        }

        public Model GetModel(int id)
        {
            return Entity.Models.SingleOrDefault(x => x.Id == id);
        }
    }
}
