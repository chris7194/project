﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class RolesRepository: ConnectionClass
    {
        public List<Role> GetAllRoles()
        {
            return Entity.Roles.ToList();
        }
    }
}
