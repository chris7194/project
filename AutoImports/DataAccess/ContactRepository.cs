﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class ContactRepository: ConnectionClass
    {
        public ContactRepository() : base() { }

        public void postmessage(ContactMessage c) {
            Entity.ContactMessages.Add(c);
            Entity.SaveChanges();
        }
    }
}
