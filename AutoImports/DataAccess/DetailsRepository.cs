﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class DetailsRepository : ConnectionClass
    {
        public DetailsRepository() : base() { }

        public void AddDetails(Detail d)
        {
            Entity.Details.Add(d);
            Entity.SaveChanges();//Whenever you are writing to database you need to call savechanges
        }
    }
}
